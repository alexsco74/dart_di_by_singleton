import 'package:dart_di_by_singleton/repositories/task.dart';

void main() {
  final task = TaskRepositoryImpl();
  final taskCopy = TaskRepositoryImpl();
  print('Is equal = ${task.hashCode == taskCopy.hashCode ? true : false}');
}
