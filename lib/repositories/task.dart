import '../models/task.dart';

abstract interface class TaskRepository {
  List<Task> get task;
}

class TaskRepositoryImpl implements TaskRepository {
  static final TaskRepositoryImpl _taskRepositoryImpl = TaskRepositoryImpl._();

  TaskRepositoryImpl._();

  final List<Task> _task = [];

  factory TaskRepositoryImpl() => _taskRepositoryImpl;

  @override
  List<Task> get task {
    return _task;
  }
}
